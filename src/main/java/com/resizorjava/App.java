package com.resizorjava;

import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.Instant;
import java.time.LocalDateTime;





public class App 
{
    

    static JFrame frame = new JFrame();
    static Map<String, Point> options = new HashMap<>();
    
    //TODO un array con opciones de extensiones de imagenes por defecto
    static ArrayList<String> optionsFormat = new ArrayList<>();
    
    public static void main( String[] args )
    {
	JPanel p = new JPanel();
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.setSize(300, 150);
	    
	    //Opciones por defecto, lo suyo seria poner resoluciones mas logicas
	    options.put("400x400", new Point(300,400));
	    options.put("640x800", new Point(640,800));
		JButton setImageSize = new JButton("Seleccionar carpeta");

		setImageSize.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent envt)
			{
			    selectOption(new JFileChooser());
			}
		    });


	    JButton close = new JButton("Salir");
	    close.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent envt)
		    {
			System.exit(0);
		    }
		});
	    

	    p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
	    p.setBorder(new EmptyBorder(10, 10, 10, 10));
	    p.add(new JLabel("Selecciona el formato deseado"));
	    p.add(close);
	    p.add(new JSeparator());


	    String[] jcomboOptions = { "jpg", "png","jpeg"};
	    for(int x =0;x < jcomboOptions.length;x++)
		{
		    JCheckBox select = new JCheckBox(jcomboOptions[x]);
		    select.addActionListener(new ActionListener(){
			    public void actionPerformed(ActionEvent evt)
			    {
				if(!select.isSelected())
				    {
					optionsFormat.remove(select.getText());

				    }else
				    {
					optionsFormat.add(select.getText());
				    }
				//	System.out.println(optionsFormat.size());
			    }
			});
		    p.add(select);
		    
		}
	    

	    
	    p.add(setImageSize);

	    frame.getContentPane().add(p);
	    frame.setLocationRelativeTo(null);
	    frame.setVisible(true);
    
	    
	    //FileNameExtensionFilter filter = new FileNameExtensionFilter("JPG and PNG", "jpg", "png");
	    //chooser.setFileFilter(filter);
	    	
    }



    public static void selectOption(JFileChooser chooser)
    {
	try{
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    chooser.setAcceptAllFileFilterUsed(false);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
		
		//TODO añadir una opcion para elegir el formato 
		File optionsFile = new File(chooser.getSelectedFile()+ File.separator +"options.json");
		ObjectMapper mapper = new ObjectMapper();
		if(optionsFile.exists()){
		    Scanner sc = new Scanner(optionsFile); 
		    LinkedHashMap<String, Point> localOptions = new ObjectMapper().readValue(sc.next(),LinkedHashMap.class);
		    // Esto es un problema con el ObjectMapper, que no detecta correctamente el tipo Point
		    // Entonces me veo obligado a mapearlo manualmente :(
		    Iterator<Map.Entry<String, Point>> optionsInterator = localOptions.entrySet().iterator();
		    while (optionsInterator.hasNext()) {
			Map.Entry<String, Point> entry = optionsInterator.next();
			entry.setValue(stringToPoint(entry.getValue() + ""));
		    }
		    
		    
		    options = localOptions;
		    
		}else{
		    
		    //TODO La primera linea de la cadena, podrian ser las opciones de formato de imagen
		    String jsonStr = mapper.writeValueAsString(options);
		    try (PrintWriter out = new PrintWriter(optionsFile.getAbsolutePath())) {
			out.println(jsonStr);
		    }
		    
		    
		}
		
		//Aqui es donde empieza la fiesta
		resizeFiles(chooser.getSelectedFile());
		
	    }
	}catch(HeadlessException e){
	    JOptionPane.showMessageDialog(frame,e.getMessage() + "\n" + e.getStackTrace(), "Error", JOptionPane.ERROR_MESSAGE); 
	    
	}catch(Exception e){
	    JOptionPane.showMessageDialog(frame,e.getMessage() + "\n" + e.getStackTrace(), "Error", JOptionPane.ERROR_MESSAGE); 
	}

    }

    public static boolean isExtension(String path)
    {
		ListIterator iterator = optionsFormat.listIterator();
		while(iterator.hasNext())
		    {
			if(path.endsWith("." + iterator.next().toString()))
				return true;
		    }

		return false;
    }

    
    public static void resizeFiles(File e) throws Exception, IOException
    {
	
	File dest = new File(e.getParentFile().getAbsolutePath() + File.separator + "resultado");
	if(dest.exists()){
	    throw new Exception("Error el archivo de destino ya existe (resultado)");
	}else{
	    dest.mkdirs();
	}
	
	
	Files.find(Paths.get(e.getPath()),
		   Integer.MAX_VALUE,
		   (filePath, fileAttr) -> fileAttr.isRegularFile() && isExtension(filePath.toString().toLowerCase()))
	    .forEach((n) -> {
		    try{
			processImg(dest, n.getFileName().toString(), n.toAbsolutePath().toString());
		    }catch(IOException b){
			throw new UncheckedIOException(b);
			//Me da la nariz que esto no esta demasiado bien
		    }catch(Exception z){
			throw new RuntimeException(z);
		    }
		});
	
    }
    
    public static void processImg(File patern,String fileName,String originalLocation)
	throws IOException,Exception
    {
	//	File niu = new File(patern.getAbsolutePath() +
	//	    File.separator  + LocalDateTime.now() +
	//	    "__" + FilenameUtils.removeExtension(fileName));

	Instant instant = Instant.now();
	int size = (instant.toEpochMilli() + "").length();
	File niu = new File(patern.getAbsolutePath() +
			    File.separator  + (instant.toEpochMilli() + "").substring(size - 5) + 
			    "__" + FilenameUtils.removeExtension(fileName));
	


	String fileLocation = niu.getPath() + File.separator + fileName;
	niu.mkdirs();
	FileUtils.copyFile(new File(originalLocation), new File(fileLocation));
	options.forEach((k, v) -> {
		try{
		    resize(new File(originalLocation), fileLocation, v.x, v.y, k);
		    
		}catch(IOException e){
		    throw new UncheckedIOException(e);
		}
	    });
	
	
    }
    
    
    public static Point stringToPoint(String spoint) throws Exception
    {
	//TODO no hay que hacer nada, pero mira que reguex mas guapa
	Pattern p = Pattern.compile("^[\\{]x=\\d{1,9}[\\.]0,\\sy=\\d{1,9}[\\.]0[\\}]$");
	Matcher c = p.matcher(spoint);
	if (c.matches())
	    {
		int x = (int)Float.parseFloat(spoint.substring(3, spoint.indexOf(",")));
		int y = (int) Float.parseFloat(spoint.substring(spoint.indexOf("y") + 2, spoint.length() - 1));
		return new Point(x,y);
		
		
	    }else{
	    throw new Exception("Error en la sintaxis de las resoluciones en JSON");
	    
	}
    }
    
    
    
    
    public static void resize(File inputFile,
			      String outputImagePath, int scaledWidth, int scaledHeight, String resolutionHelp)
	throws IOException
    {
        // reads input image
        //File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
	
        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
						      scaledHeight, inputImage.getType());
	
        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();
	
        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
						      .lastIndexOf(".") + 1);
	
        // writes to output file
	//TODO adaptar a windows
        ImageIO.write(outputImage, formatName, new File(outputImagePath + "__" + resolutionHelp + "."+formatName));
    }
    
    
    
    
}
